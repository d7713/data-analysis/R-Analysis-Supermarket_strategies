# R - Supermarket Strategies (Multivariate analysis)

### By: Andrew Wairegi

## Description
To use different multivariate analysis techniques, to identify marketing strategies
that this supermarket can use. To increase their sales. These multivariate techniques include:
dimensionality reduction, correlation, association rules, anomaly detection. Anomally detection
will be used to confirm if our findings are accurate. Using these techniques, I will be able to identify different 
marketing strategies that the supermarket can use.

[Open notebook]

## Setup/installation instructions
1. Create a local folder on your computer
2. Setup up as an empty repository (using git init)
3. Clone this repository there (git clone https://...)
4. Open R studio
5. Open the file
7. Run the file (ensure the dataset is in the same folder) 

## Known Bugs
There are no known issues / bugs

## Packages/Technologies Used
1. R - The programming language
2. Data table - A High-performance Dataframe Package
3. Tidyverse - A Data Exploration & Visualization Package
5. Corrr - A Correlation Package
6. Dplyr - A Data manipulation package
7. Arules - A Association package
8. Anomalize - A Anomaly package

<br>

### License
Copyright © 2021 **Andrew Wairegi**
<br>
You are free to view, but not copy.

[Open notebook]: https://gitlab.com/d7713/data-analysis/R-Analysis-Supermarket_strategies/-/blob/main/Supermarket_Strategies_(Multivariate_analysis).Rmd
